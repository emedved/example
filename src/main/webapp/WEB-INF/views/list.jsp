<%@ page import="java.util.List" %>
<%@ page import="org.my.example.domain.Employee" %>

<html>
<body>
 
	Function : <a href="add">Add Employee</a> |
			   <a href="list">All Employers</a>
	<hr />
	
 <table>
 <thead></thead>
 <tr>
 <td>
	<h2>Employers</h2>
	<table border="1">
		<thead>
			<tr>
				<td>First name</td>
				<td>Last name</td>
				<td>Second name</td>
				<td>Age</td>
				<td>Actions</td>
			</tr>
		</thead>
 
		<%
 
		if(request.getAttribute("employeeList")!=null){
 
			List<Employee> employeers = 
                           (List<Employee>)request.getAttribute("employeeList");
 
			if(!employeers.isEmpty()){
				 for(Employee e : employeers){
 
		%>
				<tr>
				  <td><%=e.getFirstName() %></td>
				  <td><%=e.getLastName() %></td>
				  <td><%=e.getSecondName() %></td>
				  <td><%=e.getAge() %></td>
				  <td><a href="update/<%=e.getId()%>">Update</a> |
				  	  <a href="delete/<%=e.getId() %>">Delete</a>			  
				  </td>  

				</tr>
		<%	
 
				}
 
			}
 
		   }
		%>
 
        </tr>
 
	</table>
	</td>
	<td><h2>Search by first name:</h2>
		<form method="post" action="search">
			<input type="text" style="width: 185px;" maxlength="30"
						name="first_name_search" id="first_name_search" /></span>
			<input type="submit" class="search" title="Search" value="Search" />
		</form>
	</td>	
	</tr>	
	</table>
 
</body>
</html>