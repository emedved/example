<%@ page import="org.my.example.domain.Employee" %>
<html>
<body>
	<h1>Update Employee</h1>
 
	<%
		Employee employee = new Employee();
 
		if(request.getAttribute("employee")!=null){
 
			employee = (Employee)request.getAttribute("employee");
 
		}
 
	%>
 
	<form method="post" action="../update" >
		<input type="hidden" name="id" id="id" 
			value="<%=employee.getId()%>" /> 
 
		<table>
			<tr>
				<td>
					First name :
				</td>
				<td>
					<input type="text" style="width: 185px;" 
                                             maxlength="30" name="first_name" id="first_name" 
						value="<%=employee.getFirstName() %>" />
				</td>
			</tr>
			<tr>
				<td>
					Last name :
				</td>
				<td>
					<input type="text" style="width: 185px;" 
                                             maxlength="30" name="last_name" id="last_name" 
						value="<%=employee.getLastName() %>" />
				</td>
			</tr>
			<tr>
				<td>
					Second name :
				</td>
				<td>
					<input type="text" style="width: 185px;" 
                                             maxlength="30" name="second_name" id="second_name" 
						value="<%=employee.getSecondName() %>" />
				</td>
			</tr>
			<tr>
				<td>
					Age :
				</td>
				<td>
					<input type="text" style="width: 185px;" 
                                             maxlength="30" name="age" id="age" 
						value="<%=employee.getAge() %>" />
				</td>
			</tr>
		</table>
		<input type="submit" class="update" title="Update" value="Update" />
	</form>
 
</body>
</html>