package org.my.example.controller;

import java.util.List;
import java.util.Locale;

import javax.servlet.http.HttpServletRequest;

import org.my.example.domain.Employee;
import org.my.example.service.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;


@Controller
public class TestController {
	
	@Autowired
	EmployeeService emplService;
	
	/**
	 * Simply selects the home view to render by returning its name.
	 */
	@RequestMapping(value = "/list", method = RequestMethod.GET)
	public ModelAndView list(Locale locale, Model model) {		
		
		List<Employee> employees = emplService.findAllEmployee();		
		model.addAttribute("employeeList", employees);
		
		
		return new ModelAndView("list");
	}
	
	@RequestMapping(value = "/add", method = RequestMethod.GET)
	public ModelAndView add(Locale locale, Model model) {	
		
		
		return new ModelAndView("add");
	}
	
	@RequestMapping(value = "/add", method = RequestMethod.POST)
	public ModelAndView add(HttpServletRequest request, Model model) {
 
		String firstName = request.getParameter("first_name");
		String lastName = request.getParameter("last_name");
		String secondName = request.getParameter("second_name");
		String age = request.getParameter("age");
 
		Employee e = new Employee();
		
		e.setFirstName(firstName);
		e.setLastName(lastName);
		e.setSecondName(secondName);
		e.setAge(Integer.parseInt(age));
		
		e.setExperience("junior");
		e.setDescription("ololo");
		
		
		emplService.save(e);
 
		return new ModelAndView("redirect:list"); 
	}

	@RequestMapping(value = "/update/{id}", method = RequestMethod.GET)
	public ModelAndView update(@PathVariable String id, Locale locale, Model model) {
		
		Employee e = emplService.findByEmployeeId(id);
		
		model.addAttribute("employee", e);
		
		return new ModelAndView("update");
	}
	
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public ModelAndView update(HttpServletRequest request, Model model) {
		
		String id = request.getParameter("id");
		String firstName = request.getParameter("first_name");
		String lastName = request.getParameter("last_name");
		String secondName = request.getParameter("second_name");
		String age = request.getParameter("age");
 
		Employee e = emplService.findByEmployeeId(id);
		
		e.setFirstName(firstName);
		e.setLastName(lastName);
		e.setSecondName(secondName);
		e.setAge(Integer.parseInt(age));
		
		e.setExperience("junior");
		e.setDescription("ololo"); 
		
		emplService.update(e);
 
		return new ModelAndView("redirect:list"); 
	}
	
	@RequestMapping(value = "/delete/{id}", method = RequestMethod.GET)
	public ModelAndView delete(@PathVariable String id, Locale locale, Model model) {
		
		Employee e = emplService.findByEmployeeId(id);
		emplService.delete(e);
				
		return new ModelAndView("redirect:../list");
	}

	@RequestMapping(value = "/search", method = RequestMethod.POST)
	public ModelAndView search(HttpServletRequest request, Model model) {
		
		String firstName = request.getParameter("first_name_search");
		
		List<Employee> employees = emplService.findByFirstName(firstName);		
 
		return new ModelAndView("list","employeeList",employees);
 
	}
}
