package org.my.example.service;

import java.util.List;

import org.my.example.domain.Employee;


public interface EmployeeService {
	
	void save(Employee employee);
	void update(Employee employee);
	void delete(Employee employee);
	Employee findByEmployeeId(String employeeId);
	List<Employee> findByFirstName(String firstName);
	List<Employee> findAllEmployee();

}
