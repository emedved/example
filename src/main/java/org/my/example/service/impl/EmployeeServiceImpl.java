package org.my.example.service.impl;

import java.util.List;

import org.my.example.domain.Employee;
import org.my.example.domain.dao.EmployeeDao;
import org.my.example.service.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class EmployeeServiceImpl implements EmployeeService {
	
	@Autowired
	EmployeeDao emplDao;

	public void setEmplDao(EmployeeDao emplDao) {
		this.emplDao = emplDao;
	}

	public void save(Employee employee) {
		// TODO Auto-generated method stub
		emplDao.save(employee);

	}

	public void update(Employee employee) {
		// TODO Auto-generated method stub
		emplDao.update(employee);

	}

	public void delete(Employee employee) {
		// TODO Auto-generated method stub
		emplDao.delete(employee);

	}

	public Employee findByEmployeeId(String employeeId) {
		// TODO Auto-generated method stub
		return emplDao.findByEmployeeId(employeeId);		
	}
	
	public List<Employee> findByFirstName(String firstName) {
		// TODO Auto-generated method stub
		return emplDao.findByFirstName(firstName);
	}
	
	public List<Employee> findAllEmployee() {
		// TODO Auto-generated method stub
		return emplDao.findAllEmployee();
	}

}
