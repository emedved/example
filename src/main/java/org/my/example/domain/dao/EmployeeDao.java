package org.my.example.domain.dao;

import java.util.List;

import org.my.example.domain.Employee;

public interface EmployeeDao {
	
	void save(Employee employee);
	void update(Employee employee);
	void delete(Employee employee);
	Employee findByEmployeeId(String EmployeeId);
	List<Employee> findByFirstName(String firstName);
	List<Employee> findAllEmployee();

}
