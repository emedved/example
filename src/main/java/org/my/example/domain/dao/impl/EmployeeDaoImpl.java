package org.my.example.domain.dao.impl;

import java.util.List;

import org.hibernate.SessionFactory;
import org.my.example.domain.Employee;
import org.my.example.domain.dao.EmployeeDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

@Repository
public class EmployeeDaoImpl extends HibernateDaoSupport implements EmployeeDao {
	
	@Autowired
	public void init(SessionFactory factory) {
	    setSessionFactory(factory);
	}

	public void save(Employee employee) {
		// TODO Auto-generated method stub
		getHibernateTemplate().save(employee);
		

	}

	public void update(Employee employee) {
		// TODO Auto-generated method stub
		getHibernateTemplate().update(employee);

	}

	public void delete(Employee employee) {
		// TODO Auto-generated method stub
		getHibernateTemplate().delete(employee);

	}

	public Employee findByEmployeeId(String EmployeeId) {
		// TODO Auto-generated method stub
		List list = getHibernateTemplate().find("from Employee where id = ?", Integer.parseInt(EmployeeId));
		if(!list.isEmpty())
			return (Employee)list.get(0);
		return null;
	}
	
	@SuppressWarnings("unchecked")
	public List<Employee> findByFirstName(String firstName) {
		// TODO Auto-generated method stub
		return getHibernateTemplate().find("from Employee where firstName = ?", firstName);		
	}
	
	@SuppressWarnings("unchecked")
	public List<Employee> findAllEmployee() {
		// TODO Auto-generated method stub
		return getHibernateTemplate().find("from Employee");
	}

}
