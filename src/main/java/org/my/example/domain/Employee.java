package org.my.example.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity
@Table(name = "employee")
public class Employee implements java.io.Serializable {
	
	private static final long serialVersionUID = 1L;
		
	private int id;
	private String firstName;
	private String lastName;
	private String secondName;
	private int age;
	private String description;
	private String experience;
	
	public Employee() {
	}

	public Employee(int id) {
		this.id = id;
	}
	
	public Employee(int id, String firstName, String lastName,
			String secondName, int age, String description, String experience) {
		super();
		this.id = id;
		this.firstName = firstName;
		this.lastName = lastName;
		this.secondName = secondName;
		this.age = age;
		this.description = description;
		this.experience = experience;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	public int getId() {
		return this.id;
	}
	
	public void setId(int id) {
		this.id = id;
	}

	@Column(name = "first_name", nullable = false)
	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	@Column(name = "last_name", nullable = false)
	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	@Column(name = "second_name", nullable = false)
	public String getSecondName() {
		return secondName;
	}

	public void setSecondName(String secondName) {
		this.secondName = secondName;
	}

	@Column(name = "age", nullable = false)
	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	@Column(name = "description", nullable = false)
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@Column(name = "experience", nullable = false)
	public String getExperience() {
		return experience;
	}

	public void setExperience(String experience) {
		this.experience = experience;
	}

}
